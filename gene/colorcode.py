#f = open("MN996528.flat", "r") 
#print(f.read())
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

 
filepath = 'MN996528.flat'
with open(filepath) as fp:
   line = fp.readline()
   start=0
   sequence=""
   while line:
       line = fp.readline()
       if start == 1:
           #print(line)
           for i in line:
              if i == 'a' or i == 't' or i == 'g' or i == 'c':
                 sequence+=i
                 #print(i)
       if "ORIGIN" in line:
          start=1

print(sequence)

xmax=int(len(sequence)**0.5)+1
print(xmax)
ymax=xmax

for y in range(ymax):
   for x in range(xmax):
    try:
     s=sequence[y*xmax+x]
     if str(s)=="a":
        plt.plot(x,y,'.r')
     elif str(s)=="t":
        plt.plot(x,y,'.g')
     elif str(s)=="c":
        plt.plot(x,y,'.b')
     elif str(s)=="g":
        plt.plot(x,y,'.k')
    except:
     print("exception")
   #plt.show()
plt.show()


